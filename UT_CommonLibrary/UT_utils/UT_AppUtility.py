from unittest.mock import Mock, patch

import AppOpener
import pytest
from CommonLibrary.utils.AppUtility import AppUtility

@pytest.fixture
def app_utility_instance():
    return AppUtility()


def test_get_version_number(app_utility_instance):
    with patch('AppOpener.features.mklist') as mock_mklist,\
         patch('CommonLibrary.utils.JsonUtility.JsonUtility.get_data_value_from_json_file') as mock_get_data_value_from_json_file:
        app_location = 'C:\SmartUtility'
        app_name = 'smartutility'
        app_utility_instance.get_version_number(app_location, app_name)
        mock_mklist.assert_called_once()
        mock_get_data_value_from_json_file.assert_called_once()
