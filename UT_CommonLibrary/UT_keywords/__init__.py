import sys
import os


# add path to directory Module to sys.path
source_path = os.path.abspath(os.path.join(os.path.dirname(__file__), '..', '..', 'CommonLibrary'))
sys.path.insert(0, source_path)