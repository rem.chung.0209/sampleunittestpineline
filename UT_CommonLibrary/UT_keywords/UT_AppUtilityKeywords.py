from unittest.mock import Mock, patch
import pytest
from CommonLibrary.keywords.AppUtilityKeywords import AppUtilityKeywords


@pytest.fixture
def app_utility_keyword_instance():
    return AppUtilityKeywords()


def test_get_app_version(app_utility_keyword_instance) :
    with patch('CommonLibrary.utils.AppUtility.AppUtility.get_version_number') as mock_get_app_version:
        app_name = 'SmartUtility'
        app_path = 'C:/SmartUtility'

        app_utility_keyword_instance.get_app_version(app_name, app_path)
        mock_get_app_version.assert_called_once()


def test_get_python_build_version(app_utility_keyword_instance):
    with patch('CommonLibrary.utils.AppUtility.AppUtility.get_Python_version') as mock_get_Python_build_version:
        app_utility_keyword_instance.get_Python_build_version()
        mock_get_Python_build_version.assert_called_once()


def test_get_robot_fw_version(app_utility_keyword_instance):
    with patch('CommonLibrary.utils.AppUtility.AppUtility.get_Robot_version') as mock_get_RobotFW_version:
        app_utility_keyword_instance.get_RobotFW_version()
        mock_get_RobotFW_version.assert_called_once()


def test_get_library_version(app_utility_keyword_instance):
    with patch('CommonLibrary.utils.AppUtility.AppUtility.get_library_version') as mock_get_library_version:
        lib_name = 'test_lib_name'
        app_utility_keyword_instance.get_library_version(lib_name)
        mock_get_library_version.assert_called_once()


def test_get_os_language(app_utility_keyword_instance):
    with patch('CommonLibrary.utils.AppUtility.AppUtility.get_OS_language') as mock_get_OS_language:
        app_utility_keyword_instance.get_OS_language()
        mock_get_OS_language.assert_called_once()


def test_get_os_version(app_utility_keyword_instance):
    with patch('CommonLibrary.utils.AppUtility.AppUtility.get_OS_version') as mock_get_OS_version:
        app_utility_keyword_instance.get_OS_version()
        mock_get_OS_version.assert_called_once()


def test_get_git_hash(app_utility_keyword_instance):
    with patch('CommonLibrary.utils.AppUtility.AppUtility.get_git_hash') as mock_get_git_hash:
        app_utility_keyword_instance.get_Git_Hash()
        mock_get_git_hash.assert_called_once()


def test_get_outdated_packages_list(app_utility_keyword_instance):
    with patch('CommonLibrary.utils.AppUtility.AppUtility.get_outdated_package') as mock_get_outdated_packages_list:
        path = 'C:/SmartUtility'
        file_name = 'smartUtility'
        app_utility_keyword_instance.get_outdated_packages_list(path, file_name)
        mock_get_outdated_packages_list.assert_called_once()


def test_get_all_python_packages_list(app_utility_keyword_instance):
    with patch('CommonLibrary.utils.AppUtility.AppUtility.get_all_packages') as mock_get_all_python_packages_list:
        path = 'C:/SmartUtility'
        file_name = 'smartUtility'
        app_utility_keyword_instance.get_all_python_packages_list(path, file_name)
        mock_get_all_python_packages_list.assert_called_once()


def test_initialize_start_keywords(app_utility_keyword_instance):
    with patch('robot.libraries.BuiltIn.EXECUTION_CONTEXTS.current._started_keywords_threshold') as mock_current:
        threshold_number = 1
        app_utility_keyword_instance.initialize_start_keywords(threshold_number)
        mock_current.assert_called_once()







