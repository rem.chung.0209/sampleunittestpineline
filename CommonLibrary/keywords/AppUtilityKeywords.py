from robot.api.deco import keyword

from CommonLibrary.utils.AppUtility import AppUtility
from robot.libraries.BuiltIn import EXECUTION_CONTEXTS


class AppUtilityKeywords(object):

    @keyword("Get The AUT Version")
    def get_app_version(self, app_name, app_path) -> str:
        f"""
        Get the version of application.
        == Arguments ==
        *${app_path}* (string) the application path
        *${app_name}* (string) the application name
        Example:
        | Get The AUT Version |
        """
        return AppUtility.get_version_number(self, app_path, app_name.lower())

    @keyword("Get Python Version")
    def get_Python_build_version(self) -> str:
        """
        Get the version of Python.
        == Arguments ==
        *None* \n
        Example:
        | ${python_version}=  Get Python version |
        """
        return AppUtility.get_Python_version(self)

    @keyword("Get Robot Framework Version")
    def get_RobotFW_version(self) -> str:
        """
        Get the version of Robot FW.
        == Arguments ==
        *None* \n
        Example:
        | ${robot_version}=  Get Robot Framework version |
        """
        return AppUtility.get_Robot_version(self)

    @keyword("Get ${lib_name} Library Version")
    def get_library_version(self, lib_name) -> str:
        """
        Get the version of the Library.
        == Arguments ==
        *${lib_name}* (string) The library name
        Example:
        | ${SML_version}= Get SmartUtility Library version |
        """
        return AppUtility.get_library_version(self, lib_name)

    @keyword("Get OS Language")
    def get_OS_language(self) -> str:
        """
        Get the language of operating system.
        == Arguments ==
        *None* \n
        Example:
        | ${os_lang}= Get OS language |
        """
        return AppUtility.get_OS_language(self)

    @keyword("Get OS Version")
    def get_OS_version(self) -> str:
        """
        Get the version of operating system.
        == Arguments ==
        *None* \n
        Example:
        | ${os_version}= Get OS version |
        """
        return AppUtility.get_OS_version(self)

    @keyword("Get Git Hash")
    def get_Git_Hash(self) -> str:
        """
        Get the git hash
        == Arguments ==
        *None* \n
        Example:
        | ${git_hash}= Get Git Hash |
        """
        return AppUtility.get_git_hash(self)

    @keyword("Get Outdated Packages")
    def get_outdated_packages_list(self, path, filename) -> str:
        """
        Get the list of outdated packages
        == Arguments ==
        *path* (string) The folder path
        *filename* (string) The file name
        Example:
        | ${package_list}= Get Outdated Packages |
        """
        return AppUtility.get_outdated_package(self, path, filename)

    @keyword("Get All Python Packages")
    def get_all_python_packages_list(self, path, filename) -> str:
        """
        Get the list of Python packages
        == Arguments ==
        *None* \n
        Example:
        | ${package_list}= Get All Python Packages |
        """
        return AppUtility.get_all_packages(self, path, filename)

    @keyword("Initialize Start Keywords With ${threshold_number} Threshold")
    def initialize_start_keywords(self, threshold_number) -> None:
        """
        Initialize start keywords
        == Arguments ==
        *${threshold_number}*: (int) the number of threshold
        Example:
        | Initialize Start Keywords Threshold|
        """
        EXECUTION_CONTEXTS.current._started_keywords_threshold = threshold_number
