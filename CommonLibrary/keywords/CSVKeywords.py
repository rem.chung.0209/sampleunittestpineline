import csv
from robot.api.deco import keyword


class CSVKeywords(object):

    @keyword(" Read CSV As List")
    def read_csv_as_list(self, filename, delimiter=',', encoding = None):
        output = []
        try:
            with open(filename, 'r', encoding=encoding) as file:
                csvfile = csv.reader(file, delimiter=delimiter)
                for row in csvfile:
                    output.append(row)
        except Exception as e:
            print(f'Error when openning file : {e}')

        return output

    """ Reads a given CSV file and returns it as a single list containing all values. """
    @keyword(" Read CSV As Single List")
    def read_csv_as_single_list(self, filename, delimiter=',', encoding = None):
        output = []
        try:
            with open(filename, 'r', encoding=encoding) as file:
                csvfile = csv.reader(file, delimiter=delimiter)
                for row in csvfile:
                    for value in row:
                        output.append(value)
        except Exception as e:
            print(f'Error when openning file : {e}')
        return output

    """ Reads a given CSV file and returns it as a dictionary. """
    def read_csv_as_dictionary(self, filename, key_column, value_columns, delimiter=',', encoding = None):
        output = {}
        try:
            with open(filename, 'r', encoding=encoding) as file:
                svfile = csv.DictReader(file, delimiter=delimiter)
                for row in csvfile:
                    if type(value_columns) == str:
                        output[row[key_column]] = row[value_columns]
                    elif type(value_columns) == list:
                        valueList = []
                        for value in value_columns:
                            valueList.append(row[value])
                        output[row[key_column]] = valueList
        except Exception as e:
            print(f'Error when openning file : {e}')
        return output