import json
from builtins import staticmethod
from distutils.sysconfig import get_python_lib


class JsonUtility(object):
    """JsonUtility class is used for accessing json files"""

    @staticmethod
    def get_data_value_from_json_file(file_name, key, path=None, lib_name="SmartUtilityLibrary") -> str:
        if path is None:
            site_packages_path = get_python_lib()
            path = site_packages_path + "\\" + lib_name
            file_path = ("%s\\targets\\%s.json" % (path, file_name))
        else:
            file_path = path + "\\" + file_name
        with open(file_path, mode='r') as read_file:
            data = json.load(read_file)
        return data[key]
