import os
import sys
import platform
import re
import locale
import subprocess
from AppOpener import mklist
from CommonLibrary.utils.FileUtility import FileUtility
from CommonLibrary.utils.JsonUtility import JsonUtility


class AppUtility(object):
    """AppUtility class"""

    @staticmethod
    def get_version_number(self, app_location, app_name="smartutility") -> str:
        f"""
        == Description ==
        Get the version of application
        == Arguments ==
        *${app_location}:* (string) the executable file of application
        *${app_name}:* (string) the application name
        == Returns ==
        *string* application version \n
        """
        file_path = os.path.normpath(app_location)
        mklist(name=file_path + "\\app_data.json")
        path = JsonUtility.get_data_value_from_json_file("app_data.json", app_name, file_path)

        length = len(app_name)
        firstIdx = path.lower().find(app_name)
        secondIdx = path.find('Bin')
        version = path[firstIdx + length + 1: secondIdx - 1]
        return version

    @staticmethod
    def get_app_data_path(self) -> str:
        """
        == Description ==
        Get the path of app data in Windows
        == Arguments ==
        *None*
        == Returns ==
        *string* Full path of environment APPDATA
        """
        return os.getenv('APPDATA')

    @staticmethod
    def get_Python_version(self) -> str:
        """
        == Description ==
        Get the current version of Python
        == Arguments ==
        *None*
        == Returns ==
        *string* The Python's version
        """
        return platform.python_version()

    @staticmethod
    def get_Robot_version(self) -> str:
        """
        == Description ==
        Get the current version of Robot Framework
        == Arguments ==
        *None*
        == Returns ==
        *string* The Robot Framework's version
        """
        stream = os.popen('robot --version')
        return stream.read()

    @staticmethod
    def get_library_version(self, lib_name) -> str:
        """
        == Description ==
        Get the current version of the Library
        == Arguments ==
        *${lib_name}*: (string) the library name
        == Returns ==
        *string* The Library's version
        """
        lib_path = ''
        # Get the PYTHONPATH
        for path in sys.path:
            if 'site-packages' in path:
                lib_path = path
                break
        ls_dir = FileUtility.find_folders(lib_path, lib_name)
        dist_path = ls_dir[1]
        match_string = re.search(lib_name + '-(.+?).dist-info', dist_path)
        if match_string:
            version = match_string.group(1)
            return version

    @staticmethod
    def get_OS_language(self) -> str:
        """
        == Description ==
        Get the current language of OS
        == Arguments ==
        *None*
        == Returns ==
        *string* The language of OS
        """
        language = locale.getlocale()
        return language[0]

    @staticmethod
    def get_OS_version(self) -> str:
        """
        == Description ==
        Get the current version of OS
        == Arguments ==
        *None*
        == Returns ==
        *string* The version of OS
        """
        os_sys = platform.system()
        os_ver = platform.version()
        return os_sys + " " + os_ver

    @staticmethod
    def get_git_hash(self) -> str:
        """
        == Description ==
        Get git hash
        == Arguments ==
        *None*
        == Returns ==
        *string* The git hash
        """
        git_hash = subprocess.check_output("git show -s --format=%H")
        return git_hash.decode()

    @staticmethod
    def get_outdated_package(self, path, filename) -> str:
        """
        == Description ==
        Get the list of all outdated packages
        == Arguments ==
        *path* (string) : The folder path to store file name
        *file name* (string) : The file name
        == Returns ==
        *file* The text file contains the list of outdated packages
        """
        package_list = subprocess.check_output("pip list --outdated")
        outdated_package_list = package_list.decode()
        file_path = os.path.normpath(path)
        file = open(file_path + "\\" + filename, "w")
        file.write(outdated_package_list)
        file.close()
        return file_path + "\\" + filename

    @staticmethod
    def get_all_packages(self, path, filename) -> str:
        """
        == Description ==
        Get the list of all Python packages
        == Arguments ==
        *path* (string) : The folder path to store file name
        *file name* (string) : The file name
        == Returns ==
        *file* The text file contains the list of all packages
        """
        packages_list = subprocess.check_output("pip list")
        all_packages_list = packages_list.decode()
        file_path = os.path.normpath(path)
        file = open(file_path + "\\" + filename, "w")
        file.write(all_packages_list)
        file.close()
        return file_path + "\\" + filename
