from CommonLibrary.utils.JsonUtility import JsonUtility


class DelayTimeConfig():
    """DelayTimeConfig class is used for retrieving/ customizing values of delay time"""

    def __init__(self, file_name=None):
        self._retries_control_item = None
        self._delay_time_control_item = None
        self._retries_main_window = None
        self._delay_time_main_window = None
        if file_name is None:
            self._file_name = 'delay_time_targets'
        else:
            self._file_name = file_name
        self.get_all_config()

    def get_all_config(self) -> None:
        self._delay_time_main_window = self.get_value('DELAY_TIME_MAIN_WINDOW')
        self._retries_main_window = self.get_value('RETRIES_MAIN_WINDOW')
        self._delay_time_control_item = self.get_value('DELAY_TIME_CONTROL_ITEM')
        self._retries_control_item = self.get_value('RETRIES_CONTROL_ITEM')

    def get_value(self, key) -> str:
        return JsonUtility.get_data_value_from_json_file(self._file_name, key)

    # using property decorator
    # a getter function
    @property
    def delay_time_main_window(self):
        return self._delay_time_main_window

    # a setter function
    @delay_time_main_window.setter
    def delay_time_main_window(self, value):
        self._delay_time_main_window = value

    # using property decorator
    # a getter function
    @property
    def retries_main_window(self):
        return self._retries_main_window

    # a setter function
    @retries_main_window.setter
    def retries_main_window(self, value):
        self._retries_main_window = value

    # using property decorator
    # a getter function
    @property
    def delay_time_control_item(self):
        return self._delay_time_control_item

    # a setter function
    @delay_time_control_item.setter
    def delay_time_control_item(self, value):
        self._delay_time_control_item = value

    # using property decorator
    # a getter function
    @property
    def retries_control_item(self):
        return self._retries_control_item

    # a setter function
    @retries_control_item.setter
    def retries_control_item(self, value):
        self._retries_control_item = value

    # using property decorator
    # a getter function
    @property
    def file_name(self):
        return self._file_name

    # a setter function
    @file_name.setter
    def file_name(self, name):
        self._file_name = name
        self.get_all_config()


# create a global variable
delay_time_config = DelayTimeConfig()
