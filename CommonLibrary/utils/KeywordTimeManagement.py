from datetime import datetime


class KeywordTimeManagement():
    """
    This module is managed start time and end time while execution by some keywords
    The start time is saved when calling to specific started keyword
    The end time is saved when specific ended keyword is done
    """
    
    def __init__(self):
        self._start_time = datetime.now()
        self._end_time = datetime.now()
    
    @property
    def start_time(self):
        return self._start_time
       
    # a setter function
    @start_time.setter
    def start_time(self, value):
        self._start_time = value
    
    # using property decorator
    # a getter function
    @property
    def end_time(self):
        return self._end_time
       
    # a setter function
    @end_time.setter
    def end_time(self, value):
        self._end_time = value


keywork_time_mgnt = KeywordTimeManagement()
