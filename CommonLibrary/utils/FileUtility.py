import fnmatch
import glob
import logging

from retry import retry
from robot.libraries.OperatingSystem import OperatingSystem
from datetime import datetime
import os
from win32com.client import Dispatch
import shutil
import re
import zipfile


class FileUtility(object):
    """FileUtility is used for file management"""

    @staticmethod
    @retry(tries=2)
    def copy_from_source_path_to_destination_path(src_path, dst_path) -> None:
        """
        == Description ==
        Copy all files, directories from source directory to destination directory.
        == Arguments ==
        *${src_path}:* (string) source directory
        *${dst_path}:* (string) destination directory
        """
        shutil.copytree(src_path, dst_path)

    @staticmethod
    @retry(tries=2)
    def delete_all_data_of_folder(folder_path) -> None:
        """
        == Description ==
        Delete all files, directories of directory.
        == Arguments ==
        *${folder_path}:* (string) directory path
        """
        shutil.rmtree(folder_path, ignore_errors=True)

    @staticmethod
    def search_files_with_timestamp_yyyymmdd_hhmmss(file_path, time_stamp_str, start_time_value,
                                                    end_time_value) -> list:
        """
        == Description ==
        Search files with timestamp (YYYYMMDD_HHMMSS, or YYYYMMDD-HHMMSS)
        == Arguments ==
        *${file_path}:* (string) directory path
        *${time_stamp_str}:* (string) timestamp string (YYYYMMDD_HHMMSS, or YYYYMMDD-HHMMSS)
        *${start_time_value}:* (datetime) start time
        *${end_time_value}:* (datetime) end time
        == Returns ==
        *list* list of files
        """
        time_stamp_pattern = '%Y%m%d' + time_stamp_str[8] + '%H%M%S'
                                                        
                                                           
                                                            
        file_path = os.path.normpath(file_path)
        folder_path, file_name = os.path.split(file_path)

        prefix = ''
        subfix = ''
        folder_pattern = ''
        if re.search(time_stamp_str, file_name):
            prefix = folder_path
            subfix = re.sub(time_stamp_str, '*', os.path.basename(file_path))
        else:
            is_after_time_stamp_str = False
            for name in folder_path.split(os.sep):
                if re.search(time_stamp_str, name):
                    folder_pattern = re.sub(time_stamp_str, '*', name)
                    is_after_time_stamp_str = True
                else:
                    if not is_after_time_stamp_str:
                        prefix = os.path.join(prefix, name)
                    else:
                        subfix = os.path.join(subfix, name) if subfix != '' else name

            subfix = os.path.join(subfix, file_name) if subfix != '' else file_name
        return FileUtility.get_list_existed_files(folder_pattern, prefix, subfix,
                                                  start_time_value, end_time_value, time_stamp_pattern)

    @staticmethod
    def get_list_existed_files(folder_pattern, prefix, subfix,
                               start_time_value, end_time_value, time_stamp_pattern) -> list:
        """
        == Description ==
        Get list of file within start time and end time
        == Arguments ==
        *${folder_pattern}:* (string) folder pattern
        *${prefix}:* (string) prefix
        *${subfix}:* (string) subfix
        *${time_stamp_str}:* (string) timestamp string (YYYYMMDD_HHMMSS, or YYYYMMDD-HHMMSS)
        *${start_time_value}:* (datetime) start time
        *${end_time_value}:* (datetime) end time
        == Returns ==
        *list* list of files
        """
        list_existed_files = []
        try:
            if folder_pattern != '':
                os_instance = OperatingSystem()
                os_instance.list_directories_in_directory(prefix, folder_pattern)
                for dir_name in os_instance.list_directories_in_directory(prefix, folder_pattern):
                    yyyymmdd_hhmmss = FileUtility.get_yyyymmdd_hhmmss(folder_pattern, dir_name)
                    try:
                        yyyymmdd_hhmmss = datetime.strptime(yyyymmdd_hhmmss, time_stamp_pattern)

                        if (start_time_value <= yyyymmdd_hhmmss) and (yyyymmdd_hhmmss <= end_time_value):
                            for file_name in os_instance.list_files_in_directory(os.path.join(prefix, dir_name),
                                                                                 subfix):
                                list_existed_files.append(
                                    os.path.join(prefix, dir_name, file_name))
                    except ValueError:
                        pass
            else:
                os_instance = OperatingSystem()
                for file_name in os_instance.list_files_in_directory(prefix, subfix):
                    yyyymmdd_hhmmss = FileUtility.get_yyyymmdd_hhmmss(subfix, file_name)

                    try:
                        yyyymmdd_hhmmss = datetime.strptime(yyyymmdd_hhmmss, time_stamp_pattern)
                        if (start_time_value <= yyyymmdd_hhmmss) and (yyyymmdd_hhmmss <= end_time_value):
                            list_existed_files.append(
                                os.path.join(prefix, file_name))
                    except ValueError:
                        pass
        except RuntimeError:
            pass
        return list_existed_files

    @staticmethod
    def get_yyyymmdd_hhmmss(pattern, folder_file_name) -> str:
        """
        == Description ==
        Get yyyymmdd_hhmms of folder/ file name from pattern
        == Arguments ==
        *${pattern}:* (string) pattern
        *${folder_file_name}:* (string) folder/ file name
        == Returns ==
        *string* yyyymmdd_hhmmss after remove prefix and suffix of pattern
        """
        if pattern != '*':
            yyyymmdd_hhmmss = re.sub(pattern.split('*')[0], '', folder_file_name)
            yyyymmdd_hhmmss = re.sub(pattern.split('*')[1], '', yyyymmdd_hhmmss)
        else:
            yyyymmdd_hhmmss = folder_file_name
        return yyyymmdd_hhmmss

    @staticmethod
    def save_images_from_rtf_file(rtf_file) -> list:
        """
        Save image from rtf file to the same folder \n
        == Arguments ==
        *${rtf_file}:* (string) rtf file \n
        == Returns ==
        *${list}:* of image files are saved \n
        """
        # save rtf file to docx file
        word = Dispatch('word.application')
        word.DisplayAlerts = 0
        word.visible = 0
        doc = word.Documents.Open(rtf_file)
        docxpath = rtf_file.replace('rtf', 'docx')
        doc.SaveAs(docxpath, 12)
        doc.Close()
        word.Quit()

        # copy image
        doc = zipfile.ZipFile(docxpath)
        despath = os.path.dirname(rtf_file)
        image_files = []
        for info in doc.infolist():
            if info.filename.endswith((".png", ".jpeg", ".gif")):
                doc.extract(info.filename, despath)
                image_file = despath + "\\" + info.filename.split("/")[-1]
                shutil.copy(despath + "\\" + info.filename, image_file)
                image_files.append(image_file)
        doc.close()
        return image_files

    @staticmethod
    def copy_file_from_source_path_to_destination_path(file_name, src_path, dst_path) -> None:
        """
        == Description ==
        Copy the file from source directory to destination directory.
        == Arguments ==
        *${file_name}:* (string) source directory
        *${src_path}:* (string) source directory
        *${dst_path}:* (string) destination directory
        """
        orig_path = os.path.normpath(src_path + "\\" + file_name)
        target_path = os.path.normpath(dst_path + "\\" + file_name)
        shutil.copyfile(orig_path, target_path)

    @staticmethod
    def delete_file_of_folder(file_name, folder_path) -> None:
        """
        == Description ==
        Delete a file in the folder
        == Arguments ==
        *${file_name}:* (string) file name
        *${folder_path}:* (string) folder path
        """
        file_path = os.path.normpath(folder_path + "\\" + file_name)
        if os.path.isfile(file_path):
            os.remove(file_path)
        else:
            print("Error: %s file not found" % file_name)

    @staticmethod
    def check_folder_exists(folder_path, folder_name) -> bool:
        """
        == Description ==
        Check folder exists
        == Arguments ==
        *${folder_name}:* (string) folder name
        *${folder_path}:* (string) folder path
        == Returns ==
        *True* if folder exists, otherwise, return *False*
        """
        dir_path = os.path.normpath(folder_path + "\\" + folder_name)
        isExist = os.path.exists(dir_path)
        return isExist

    @staticmethod
    def check_file_name_exists(path, file_name) -> bool:
        """
        == Description ==
        Check file name exists
        == Arguments ==
        *${path}:* (string) the folder path
        *${file_name}:* (string) the file name
        == Returns ==
        *True* if file exists, otherwise, return *False*
        """
        file_path = os.path.normpath(path + "\\" + file_name)
        isExist = os.path.exists(file_path)
        return isExist

    @staticmethod
    def create_folder_names(folder_path, folder_name) -> None:
        """
        == Description ==
        Create folder
        == Arguments ==
        *${folder_path}:* (string) the folder path
        *${folder_name}:* (string) the folder name
        == Returns ==
        *None*
        """
        logging.info("Folder path: %s", folder_path)
        logging.info("Folder name: %s", folder_name)
        normal_path = os.path.normpath(folder_path + "\\" + folder_name)
        logging.info("Path: %s", normal_path)
        os.mkdir(normal_path)

    @staticmethod
    def delete_folder(folder_path, folder_name) -> None:
        """
        == Description ==
        Delete folder and files if exists
        == Arguments ==
        *${folder_path}:* (string) the folder path
        *${folder_name}:* (string) the folder name
        == Returns ==
        *None*
        """
        normal_path = os.path.normpath(folder_path + "\\" + folder_name)
        try:
            shutil.rmtree(normal_path)
        except OSError as e:
            print("Error: %s - %s." % (e.filename, e.strerror))

    @staticmethod
    def find_folders(base, pattern) -> list:
        """
        == Description ==
        Return list of sub folders matching pattern in base folder.
        == Arguments ==
        *${base}:* (string) the base folder
        *${pattern}:* (string) the pattern
        == Returns ==
        *List of sub folders if found. Otherwise, return the empty list*
        """
        dir_list = []
        for file in os.listdir(base):
            sub_dir = os.path.join(base, file)
            if os.path.isdir(sub_dir) and pattern in sub_dir:
                dir_list.append(sub_dir)
        return dir_list

    @staticmethod
    def search_files_with_pattern(base, pattern) -> list:
        """
        == Description ==
        Return list of files matching pattern in base folder.
        == Arguments ==
        *${base}:* (string) the base folder
        *${pattern}:* (string) the pattern
        == Returns ==
        *List of files if found. Otherwise, return the empty list*
        """
        os.chdir(base)
        file_list = glob.glob("*/" + pattern)
        return file_list

    @staticmethod
    def count_number_of_files_in_folder(base, criteria) -> int:
        """
        == Description ==
        Return the number of files in base folder.
        == Arguments ==
        *${base}:* (string) the base folder
        *${criteria}:* (string) the filter criteria
        == Returns ==
        *The number of files in the base folder*
        """
        os.chdir(base)
        dir_list = os.listdir(base)
        os.chdir(dir_list[0])
        full_path = base + "\\" + dir_list[0]
        if len(criteria) == 0:
            no_of_files = len(os.listdir(full_path))
        else:
            no_of_files = len(fnmatch.filter(os.listdir(full_path), '*' + criteria + '*'))
        return no_of_files

    @staticmethod
    def check_file_exists_with_pattern(base, pattern) -> bool:
        """
        == Description ==
        Verify that the list of files matching pattern in base folder.
        == Arguments ==
        *${base}:* (string) the base folder
        *${pattern}:* (string) the pattern
        == Returns ==
        *True* if List of files is found. Otherwise, return the *False*
        """
        is_Exist = False
        file_list = FileUtility.search_files_with_pattern(base, pattern)
        if len(file_list) > 0:
            is_Exist = True
        return is_Exist

    @staticmethod
    def check_any_folder_exists(current_working_folder) -> bool:
        """
        == Description ==
        Verify that any folder exists in the current working folder.
        == Arguments ==
        *${current_working_folder}:* (string) the current working folder
        == Returns ==
        *True* if current working folder contains any folder. Otherwise, return the *False*
        """
        for item in os.listdir(current_working_folder):
            if os.path.isdir(item):
                return True
            else:
                return False

    @staticmethod
    def check_any_files_exists(current_working_folder) -> bool:
        """
        == Description ==
        Verify that any files exists in the current working folder.
        == Arguments ==
        *${current_working_folder}:* (string) the current working folder
        == Returns ==
        *True* if current working folder contains any files. Otherwise, return the *False*
        """
        for item in os.listdir(current_working_folder):
            if os.path.isfile(item):
                return True
            else:
                return False
