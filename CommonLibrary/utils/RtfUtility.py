from win32com.client import Dispatch
from CommonLibrary.utils.DateTimeUtility import DateTimeUtility


class RtfUtility():
    """RtfUtility class is used for accessing SmartUtility report"""

    def __init__(self, rtf_file_path) -> None:
        """
        == Description ==
        Initialize by the rft file.
         == Arguments ==
        *${rtf_file_path}:* (string) the rth file path
        """
        self.word = Dispatch('Word.Application')
        self.word.Visible = 0
        self.word.DisplayAlerts = 0
        self.doc = self.word.Documents.Open(FileName=rtf_file_path, Encoding='gbk')

    def __del__(self) -> None:
        """
        == Description ==
        Destructor after processing the rtf file.
        """
        self.doc.Close()
        self.word.Quit()

    def get_data_by_search_value(self, search_value) -> str:
        """
        == Description ==
        Get data by the search value from the rft file.
         == Arguments ==
        *${search_value}:* (string) the search value
        == Returns ==
        The data after searching in the rtf file
        """
        search_value = search_value + ':'
        is_existing = False
        for para in self.doc.paragraphs:
            if is_existing is False:
                if str(para.Range.Text).lower().startswith(str(search_value).lower()):
                    is_existing = True
            else:
                return str(para.Range.Text).strip()
        return ''

    def verify_data_period(self, data_period_value) -> (bool, str):
        """
        == Description ==
        Verify whether data period value is existing in SmartUtility report or not.
         == Arguments ==
        *${data_period_value}:* (string) the input data_period value
        == Returns ==
        *True* if data_period value is existing in SmartUtility report
        *False* if data_period value is not existing in SmartUtility report
        """
        data_period_actual = self.get_data_by_search_value('Data period').replace("", '').strip()
        return data_period_actual.__eq__(data_period_value.strip()), data_period_actual

    def verify_report_date(self, report_date_value) -> (bool, str):
        """
        == Description ==
        Verify whether report date value is existing in SmartUtility report or not.
         == Arguments ==
        *${report_date_value}:* (string) the input report date value
        == Returns ==
        *True* if report date value is existing in SmartUtility report
        *False* if report date value is not existing in SmartUtility report
        *str* actual value of report date
        """
        report_date_actual = self.get_data_by_search_value('Report date').replace("", '').strip()
        return report_date_actual.__eq__(report_date_value), report_date_actual

    def verify_device_information(self, devices, database_values) -> [bool, dict]:
        """
        == Description ==
        Verify whether key information is existing in SmartUtility report or not.
         == Arguments ==
        *${device_name}:* (string) the input key name
        *${ip_address}:* (string) the input ip address
        *${serial_number}:* (string) the input serial number
        *${download_data}:* (string) the input download data
        *${firmware}:* (string) the input firmware
        == Returns ==
        *bool* True if key information is existing in SmartUtility report, otherwise return False
        *dict* dictionary for not same items
        """
        report_values = {}
        not_same_values = {}
        for word_table in self.doc.Tables:
            table = []
            for word_row in word_table.Rows:
                row = [cell.Range.Text for cell in word_row.Cells]
                table.append(row)
                if ("['IP address" in str(table) and
                        "['Serial number" in str(table) and
                        "['Data download" in str(table) and
                        "['Firmware" in str(table)):
                    current_device_name = current_ip_address = current_serial_number = current_firmware = ''
                    for item in table:
                        if '/' in str(item[0]).replace("", ''):
                            current_device_name = str(item[1]).replace("", '').strip()
                        elif 'IP address' in str(item[0]).replace("", ''):
                            current_ip_address = str(item[1]).replace("", '').strip()
                        elif 'Serial number' in str(item[0]).replace("", ''):
                            current_serial_number = str(item[1]).replace("", '').strip()
                        elif 'Firmware' in str(item[0]).replace("", ''):
                            current_firmware = str(item[1]).replace("", '').strip()
                    if current_device_name in devices:
                        report_values[current_device_name] = [current_ip_address,
                                                              current_serial_number,
                                                              current_firmware]
                        # only devices which are not in report will be remain
                        devices.remove(current_device_name)
        for key in report_values.keys():
            if key in database_values and report_values[key] != database_values[key]:
                not_same_values[key] = "In report: %s. In database: %s" % (report_values[key], database_values[key])
            elif key not in database_values:
                not_same_values[key] = "Not found in database"
        for key in devices:
            if key not in database_values:
                not_same_values[key] = "Not found in database and report"
            else:
                not_same_values[key] = "Not found in report"

        return not not_same_values, not_same_values

    def verify_log_book(self, devices, database_values, start_timestamp, end_timestamp) -> (bool, dict):
        """
        == Description ==
        Verify whether the log book is existing in SmartUtility period_report or not.
         == Arguments ==
        *${devices}:* (string) the input device names
        *${database_values}:* (dict) database values
        *${start_timestamp}:* (float) the start time
        *${end_timestamp}:* (float) the end time
        == Returns ==
        *bool* True if the log book is existing in SmartUtility period_report, otherwise return False
        *dict* Key is device name and value is list of not same periods
        """
        report_values = {}
        for name in devices:
            report_values[name] = {('Created', 'Changed'): ['User', 'Message']}
        for word_table in self.doc.Tables:
            table = []
            for word_row in word_table.Rows:
                row = [cell.Range.Text for cell in word_row.Cells]
                table.append(row)
            if ("'Created" in str(table) and "'Changed" in str(table)
                    and "'User" in str(table) and "'Message" in str(table)):
                # each table, index = 0 is device name, index = 1 is table column
                report_device_name = str(table[0][0]).replace("\r", " ").replace("\x07", " ").strip()
                if report_device_name in devices:
                    devices.remove(report_device_name)
                    values = report_values[report_device_name]
                    for index in range(2, len(table)):
                        values = self.parse_logbook_detail(values, table[index], start_timestamp, end_timestamp)
                    report_values[report_device_name] = values
        not_same_values = self.compare_logbook_dictionary_between_report_and_database(report_values, database_values,
                                                                                      devices)
        return not not_same_values, not_same_values

    def parse_logbook_detail(self, values, logbook_row, start_timestamp, end_timestamp) -> dict:
        """
        == Description ==
        Parse list of logbook to dictionary
         == Arguments ==
        *${logbook_row}:* (dict) logbook data
        *${start_timestamp}:* (int) start time
        *${end_timestamp}:* (int) end time
        == Returns ==
        *dict* Key is start time, end time, value is message
        """
        report_created_datetime = str(logbook_row[1]).replace("\r", " ").replace("\x07", " ").strip()
        report_changed_datetime = str(logbook_row[2]).replace("\r", " ").replace("\x07", " ").strip()
        report_user = str(logbook_row[3]).replace("\r", " ").replace("\x07", " ").strip()
        report_message = str(logbook_row[4]).replace("\r", " ").replace("\x07", " ").strip()

        if start_timestamp is None or end_timestamp is None:
            values[report_created_datetime, report_changed_datetime] = [report_user, report_message]
        elif not ''.__eq__(report_created_datetime):
            report_create_datetime_timestamp = DateTimeUtility().parse_database_timestamp_from_default_local_datetime(
                report_created_datetime)
            if start_timestamp <= report_create_datetime_timestamp <= end_timestamp:
                values[report_created_datetime, report_changed_datetime] = [report_user, report_message]
        else:
            values = []
        return values

    def compare_logbook_dictionary_between_report_and_database(self, report_values, database_values,
                                                               devices_not_in_report) -> dict:
        """
        == Description ==
        Compare 2 dictionaries of logbook
         == Arguments ==
        *${report_values}:* (dict) the report values
        *${database_values}:* (dict) the database 
        *${devices_not_in_report}:* (list) list of device which is not in report
        == Returns ==
        *dict* Key is device name and value is list of not same periods
        """
        not_same_values = {}
        for name in report_values:
            not_same_values[name] = []
            db = database_values[name]
            period_report = report_values[name]
            if len(db) > 1:
                for period in period_report:
                    row_value = not_same_values[name]
                    if period in db:
                        if db[period] != period_report[period]:
                            row_value.append(
                                "%s : %s AND %s" % (' to '.join(period), period_report[period], db[period]))
                            # only period which is not in report will be remain in dbafter this loop
                        db.pop(period)
                    else:
                        row_value.append("%s is not found in database" % (' to '.join(period)))
                    not_same_values[name] = row_value
                # if db is not empty, it means that it is not found in report
                for period in db:
                    row_value = not_same_values[name]
                    row_value.append("%s is not found in report. Values: %s" % (' to '.join(period), db[period]))
                    not_same_values[name] = row_value
            elif len(report_values[name]) != 0:
                not_same_values[name] = ["Not found in database"]
            if not not_same_values[name]:
                not_same_values.pop(name)
        for name in devices_not_in_report:
            if name not in database_values:
                not_same_values[name] = ["Not found in database and period_report"]
            else:
                not_same_values[name] = ["Not found in report"]
        return not_same_values

    def parse_meausrement_data(self) -> list:
        """
        == Description ==
        Parse measurement data from table measurement
        == Returns ==
        *list* Key is device name and value is list of not same periods
        """
        for word_table in self.doc.Tables:
            table = []
            for word_row in word_table.Rows:
                row = [cell.Range.Text.replace("\r\x07", "") for cell in word_row.Cells]
                table.append(row)
            if "'x-values" in str(table).lower() and "'y-values" in str(table).lower():
                for row in table[1:]:
                    row[0] = DateTimeUtility().convert_utc_string_to_date_time(row[0])
                    row[1] = round(float(row[1]) * 1000, 3)
                return table
