from datetime import datetime, timedelta
from dateutil.parser import parse
import locale


class DateTimeUtility(object):
    """DateTimeUtility is used for converting datetime, timestamp"""
    _DELTA_TIME_FROM_0001_1_1_TO_1970_1_1 = 62135596800

    def convert_from_pc_timestamp_to_db_timestamp(self, timestamp) -> float:
        """
        == Description ==
        Convert time stamp to database timestamp
        == Arguments ==
        *${timestamp}:* (int) timestamp value
        == Returns ==
        *float* database timestamp \n
        """
        return (timestamp + self._DELTA_TIME_FROM_0001_1_1_TO_1970_1_1) * 10000000

    def convert_from_db_timestamp_to_pc_timestamp(self, timestamp) -> float:
        """
        == Description ==
        Convert time stamp to pc timestamp
        == Arguments ==
        *${timestamp}:* (int) timestamp value
        == Returns ==
        *float* PC timestamp \n
        """
        return float(timestamp) / 10000000 - self._DELTA_TIME_FROM_0001_1_1_TO_1970_1_1

    def format_to_default_locale_datetime_by_timestamp(self, time_stamp_str) -> str:
        """
        == Description ==
        Format timestamp to default locale
        == Arguments ==
        *${time_stamp_str}:* (string) timestamp string
        == Returns ==
        *str* date time of default locale \n
        """
        date_time = datetime.fromtimestamp(time_stamp_str)
        default_locale = locale.getlocale()[0]
        locale.setlocale(locale.LC_ALL, default_locale)
        return date_time.strftime("%c")

    def format_to_default_locale_by_datetime(self, date_time, pattern) -> str:
        """
        == Description ==
        Format timestamp to default locale
        == Arguments ==
        *${time_stamp_str}:* (string) timestamp string
        == Returns ==
        *str* date time of default locale \n
        """
        default_locale = locale.getlocale()[0]
        locale.setlocale(locale.LC_ALL, default_locale)
        return date_time.strftime(pattern)

    def parse_datetime_from_default_locale_date(self, date_str, pattern) -> datetime:
        """
        == Description ==
        Parse to datetime from datetime string
        == Arguments ==
        *${time_stamp_str}:* (string) timestamp string
        == Returns ==
        *datetime* datetime value \n
        """
        default_locale = locale.getlocale()[0]
        locale.setlocale(locale.LC_ALL, default_locale)
        return datetime.strptime(date_str, pattern)

    def parse_database_timestamp_from_default_local_date(self, date_str) -> float:
        """
        == Description ==
        Parse to string of date which format is default locale to database timestamp
        == Arguments ==
        *${datetime_str}:* (string) datetime string, which format is default locale
        == Returns ==
        *float* timestamp value \n
        """
        date_time = self.parse_datetime_from_default_locale_date(date_str, "%x")
        return self.convert_from_pc_timestamp_to_db_timestamp(date_time.timestamp())

    def parse_database_timestamp_from_default_local_datetime(self, datetime_str) -> float:
        """
        == Description ==
        Parse to string of date which format is default locale to database timestamp
        == Arguments ==
        *${date_str}:* (string) date string, which format is default locale
        == Returns ==
        *float* timestamp value \n
        """
        date_time = self.parse_datetime_from_default_locale_date(datetime_str, "%c")
        return self.convert_from_pc_timestamp_to_db_timestamp(date_time.timestamp())

    def convert_utc_string_to_date_time(self, utc_string) -> str:
        """
        == Description ==
        Convert utc string to datetime
        == Arguments ==
        *${utc_string}:* (string) the utc string
         == Returns ==
        *str* date time with default locale format
        """
        initial_datetime = datetime(1900, 1, 1)
        converted_datetime = initial_datetime \
                             + timedelta(days=(float(utc_string) - 2), hours=7)
        date_time_parse = parse(str(converted_datetime)[:str(converted_datetime).find('.')])
        default_locale = locale.getlocale()[0]
        locale.setlocale(locale.LC_ALL, default_locale)
        return datetime.strftime(date_time_parse, '%c')