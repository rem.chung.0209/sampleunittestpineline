import sqlite3


class DBUtility(object):
    """DBUtility is used for querying database"""

    @staticmethod
    def execute_select_statement_from_db(db_file, select_statement, data) -> list:
        """
        == Description ==
        Execute select statement on database
        == Arguments ==
        *${db_file}:* (string) path of database file
        *${select_statement}:* (string) select statement string
        *${data}:* (list) list of data to select
        == Returns ==
        *list* list of query results \n
        """
        conn = None
        query_results = None
        try:
            conn = sqlite3.connect(db_file)
            cur = conn.cursor()
            cur.execute(select_statement, data)
            query_results = cur.fetchall()
            conn.close()
        except Exception as e:
            print(e)
        return query_results
