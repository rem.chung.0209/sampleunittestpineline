from robotlibcore import DynamicCore
from CommonLibrary.keywords.AppUtilityKeywords import AppUtilityKeywords
from CommonLibrary.keywords.CSVKeywords import CSVKeywords

class CommonLibrary(DynamicCore):
    """CommonLibrary class is used for automating CommonLibrary application"""

    def __init__(self):
        libraries = [
            AppUtilityKeywords(),
            CSVKeywords()
        ]
        DynamicCore.__init__(self, libraries)
