import os
import psutil
from AppOpener import open


class Launch(object):
    """Launch on SmartUtility application"""

    @staticmethod
    def open_process(app_name, main_window_name) -> None:
        """
        == Description ==
        Open SmartUtility process.
        If SmartUtility process is opening, this process will be closed and opened again.
        == Arguments ==
        *${app_name}:* (string) the application name
        *${main_window_name}:* (string) the main window name of this application
        """
        for proc in psutil.process_iter():
            if str(main_window_name).lower() in proc.name().lower():
                proc.kill()
                break
        open(app_name)

    @staticmethod
    def execute_cmd(cmd_str) -> None:
        """
        == Description ==
        Execute command
        == Arguments ==
        *${cmd_str}:* (string) command string
        """
        os.system(cmd_str)
