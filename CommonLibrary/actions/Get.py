import logging

from retry import retry
from CommonLibrary.utils.DelayTimeConfig import delay_time_config
import uiautomation as ui
import psutil
from uiautomation.uiautomation import ControlType


class Get(object):
    """Get element on SmartUtility application"""

    @staticmethod
    @retry(tries=2)
    def text_control_by_name(text_expected) -> bool:
        """
        == Description ==
        Get the text-control by text_expected in SmartUtility application.
        If getting failed at the first time, this action will
        be triggered again.
         == Arguments ==
        *${text_expected}:* (string) the expected text
        == Returns ==
        *True* if the expected text control is displayed
        *False* if the expected text control is not displayed
        """
        return ui.TextControl(Name=text_expected, foundIndex=1).Name is not None

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item)
    def name_of_text_control_by_id(automation_id, index=1) -> str:
        logging.info("Index value: %s", index)
        return ui.TextControl(AutomationId=automation_id, foundIndex=index).Name

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item)
    def legacy_name_of_text_control_by_id(automation_id) -> str:
        return ui.TextControl(AutomationId=automation_id, foundIndex=1).GetLegacyIAccessiblePattern().Name

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item)
    def window_by_id(automation_id) -> bool:
        """
        == Description ==
        The exists attribute of the window name based on the automation id in SmartUtility application.
        If getting failed at the first time, this action will
        be triggered again.
        == Arguments ==
        *${automation_id}:* (string) the automation id
        == Returns ==
        *True* if window is opened
        *False* if window is closed
        """
        return ui.WindowControl(AutomationId=automation_id).Exists()

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item)
    def id_of_first_sub_window(automation_id) -> str:
        """
        == Description ==
        The exists attribute of the window name based on the automation id in SmartUtility application.
        If getting failed at the first time, this action will
        be triggered again.
        == Arguments ==
        *${automation_id}:* (string) the automation id
        == Returns ==
        *True* if window is opened
        *False* if window is closed
        """
        first_sub_window_id = ''
        first_child = ui.WindowControl(AutomationId=automation_id).GetFirstChildControl()
        if first_child.ClassName == 'Window':
            first_sub_window_id = first_child.AutomationId
        return first_sub_window_id

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item)
    def name_of_window_by_id(automation_id) -> str:
        return ui.WindowControl(AutomationId=automation_id).Name

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item)
    def value_of_edit_control_by_id(automation_id):
        """
        == Description ==
        Get the value of edit-control based on the automation id in SmartUtility application.
        If getting failed at the first time, this action will
        be triggered again.
        == Arguments ==
        *${automation_id}:* (string) the automation id
        == Returns ==
        The value of the edit control
        """
        return ui.EditControl(AutomationId=automation_id,
                              foundIndex=1).GetValuePattern().Value

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item)
    def selected_state_of_data_item_by_custom_control_automation_id(data_item_name,
                                                                    select_item_custom_control_automation_id) -> bool:
        """
        == Description ==
        Get toggle value of data item
        == Arguments ==
        *${data_item_name}:* (string) data name
        *${select_item_custom_control_automation_id}:* (string) custom control automation id of select item
        == Returns ==
        *True* if data name is selected
        *False* if data name is not selected
        """
        data_item = ui.DataItemControl(Name=data_item_name).CustomControl(
            AutomationId=select_item_custom_control_automation_id)
        return data_item.Name == 'True'

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item)
    def toggle_state_of_checkbox_id(automation_id) -> int:
        """
        == Description ==
        Get toggle value of checkbox
        == Arguments ==
        *${automation_id}:* (string) checkbox automation id
        == Returns ==
        *On (1)* if checkbox is checked
        *Off (0)* if checkbox is unchecked
        """
        return ui.CheckBoxControl(AutomationId=automation_id, foundIndex=1).GetTogglePattern().ToggleState

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item)
    def devices_information(data_grid_automation_id, start_col_idx=0) -> dict:
        """
        == Description ==
        Get value of device information table
        == Arguments ==
        *${data_grid_automation_id}:* (string) list result automation id
        *${start_col_idx}:* (int) start column index. It starts from 0
        == Returns ==
        (dictionary) device name and result value
        """
        rows_data = {}
        list_results = ui.DataGridControl(AutomationId=data_grid_automation_id)
        list_results.SetFocus()
        rows = list_results.GetChildren()
        headers = rows[0].GetChildren()
        row_index = 1
        while row_index < len(rows):
            cell_data = {}
            column_index = start_col_idx
            cells = rows[row_index].GetChildren()
            while column_index < len(cells):
                header_name = headers[column_index].Name
                # value = cells[column_index].CustomControl().Name
                if len(cells[column_index].GetChildren()):
                    value = cells[column_index].GetChildren()[0].Name
                else:
                    value = cells[column_index].Name
                cell_data[header_name] = value.replace('\n', '')
                column_index += 1
            rows_data[cell_data['Device name']] = cell_data
            row_index += 1
        return rows_data

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item)
    def value_of_radio_button_of_list_item_using_automation_id(automation_id, found_index: int = 1) -> bool:
        """
        == Description ==
        Get value of radio button
        == Arguments ==
        *${automation_id}:* (string) radio automation id
        == Returns ==
        *True* if radio is checked
        *False* if radio is unchecked
        """
        return (ui.ListItemControl(AutomationId=automation_id, foundIndex=found_index)
                .RadioButtonControl(foundIndex=1).GetSelectionItemPattern().IsSelected)

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item)
    def date_picker_value_using_automation_id(automation_id, found_index=1) -> str:
        """
        == Description ==
        Get value of date picker
        == Arguments ==
        *${automation_id}:* (string) date picker automation id
        == Returns ==
        (str) Value of data picker
        """
        return ui.CustomControl(AutomationId=automation_id, foundIndex=found_index).GetLegacyIAccessiblePattern().Value

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item)
    def all_names_of_tree_viewer_by_automation_id(tree_control_pane_automation_id, device_list_automation_id,
                                                  found_index=1) -> list:
        """
        == Description ==
        Get list value of name of tree viewer
        == Arguments ==
        *${tree_control_pane_automation_id}:* (string) pane control automation of tree
        *${device_list_automation_id}:* (string) automation id of device list, this item is child of tree control pane
        == Returns ==
        (list) Value of all items at tree
        """
        names = []
        # tree = ui.PaneControl(AutomationId=tree_control_pane_automation_id, foundIndex=found_index)
        tree = ui.TreeControl(AutomationId=tree_control_pane_automation_id, foundIndex=found_index)
        logging.info("Get tree item: %s", tree.GetChildren())
        # for child in tree.GetChildren():
        #   if child.AutomationId == device_list_automation_id:
        #      for item in child.GetChildren():
        #         names.append(item.Name)
        # logging.info("Get list item: %s", names)
        return names

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item)
    def selected_name_item_of_combo_box_automation_id(combo_box_automation_id, found_index=1) -> str:
        f"""
        == Description ==
        Get name of selected item at combo box
        == Arguments ==
        *${combo_box_automation_id}:* (string) combo box automation id
        *${found_index}:* (int) the index of combo box
        == Returns ==
        (string) name of selected item
        """
        if len(ui.ComboBoxControl(AutomationId=combo_box_automation_id,
                                  foundIndex=found_index).GetSelectionPattern().GetSelection()) > 0:
            name = ui.ComboBoxControl(AutomationId=combo_box_automation_id,
                                      foundIndex=found_index).GetSelectionPattern().GetSelection()[0].Name
        else:
            name = ''
        return name

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item)
    def list_items_of_combo_box_automation_id(combo_box_automation_id, found_index: int = 1) -> list:
        f"""
        == Description ==
        Get all item names of combo box
        == Arguments ==
        *${combo_box_automation_id}:* (string) combo box automation id
        *${found_index}:* (int) the index of combo box
        == Returns ==
        (list) item names
        """
        lst = []
        combo_box = ui.ComboBoxControl(AutomationId=combo_box_automation_id, foundIndex=found_index)
        combo_box.Click()
        for item in combo_box.GetChildren():
            lst.append(item.Name)
        return lst

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item)
    def enable_status_checkbox_of_list_device_by_automation_id(data_grid_automation_id) -> bool:
        """
        == Description ==
        Get enable status checkbox of list device
        == Arguments ==
        *${data_grid_automation_id}:* (string) data grid automation id
        == Returns ==
        (True) if all items have enable status
        (False) if any item has disable status
        """
        list_devices = ui.DataGridControl(AutomationId=data_grid_automation_id)
        list_devices.SetFocus()
        index = 1
        rows = list_devices.GetChildren()
        isEnable = True
        while index < len(rows):
            if not rows[index].CheckBoxControl(AutomationId='ItemCheckBox').IsEnabled:
                isEnable = False
                break
            else:
                index += 1
        return isEnable

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item)
    def list_items_of_list_control_automation_id(list_control_automation_id) -> list:
        """
        == Description ==
        Get all item names of list control
        == Arguments ==
        *${list_control_automation_id}:* (string) list control automation id
        == Returns ==
        (list) item names
        """
        list_items = []
        list_view = ui.ListControl(AutomationId=list_control_automation_id)
        list_view.SetFocus()
        for item in list_view.GetChildren():
            list_items.append(item.Name)
        return list_items

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item)
    def selected_item_name_of_list_control_automation_id(list_control_automation_id) -> str:
        """
        == Description ==
        Get name of selected item of list control
        == Arguments ==
        *${list_control_automation_id}:* (string) list control automation id
        == Returns ==
        (list) item names
        """
        return ui.ListControl(AutomationId=list_control_automation_id).GetSelectionPattern().GetSelection()[0].Name

    @staticmethod
    def file_paths_opened_by_application(application_name) -> list:
        """
        == Description ==
        Get paths of all files which are opened by application (for example: winword.exe, excel.exe)
        == Arguments ==
        *${application_name}:* (string) application name
        == Returns ==
        (list) file paths
        """
        file_paths = []
        for proc in psutil.process_iter():
            if proc.name().lower() == application_name.lower():
                for open_file in proc.open_files():
                    path = getattr(open_file, 'path')
                    file_paths.append(path)
        return file_paths

    @staticmethod
    def edit_control_values_of_custom_control_automation_id(custom_control_automation_id) -> list:
        """
        == Description ==
        Get list values for all custom controls which are child of custom control
        == Arguments ==
        *${custom_control_automation_id}:* (string) custom control automation id
        == Returns ==
        (list) values for all edit controls
        """
        values = []
        controls = ui.CustomControl(AutomationId=custom_control_automation_id).GetChildren()
        for item_control in controls:
            if item_control.ClassName == 'TextBox':
                values.append(item_control.GetValuePattern().Value)
        return values

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item)
    def name_of_container_by_id(automation_id) -> str:
        """
        == Description ==
        Get the name of container based on the automation id in SmartUtility application.
        If getting failed at the first time, this action will
        be triggered again.
        == Arguments ==
        *${automation_id}:* (string) the container's automation id
        == Returns ==
        The value of the edit control
        """
        return ui.GroupControl(AutomationId=automation_id).Name

    @staticmethod
    def tooltip_of_custom_control_automation_id(custom_control_automation_id, parent_custom_control_automation_id,
                                                found_index=1) -> str:
        """
        == Description ==
        Get tooltip of custom control by help text attribute
        == Arguments ==
        *${parent_custom_control_automation_id}:* (string) parent custom control automation id
        *${custom_control_automation_id}:* (string) custom control automation id
        == Returns ==
        (str) help text value of custom control
        """
        if parent_custom_control_automation_id:
            edit_control = ui.CustomControl(AutomationId=parent_custom_control_automation_id, foundIndex=found_index)
            return edit_control.CustomControl(AutomationId=custom_control_automation_id,
                                              foundIndex=found_index).HelpText

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item)
    def existed_status_of_panel_name_on_window_id(panel_name, window_id, found_index=1) -> bool:
        """
        == Description ==
        Get existed status of panel name on the window id
        == Arguments ==
        *${panel_name}:* (string) the panel name
        *${window_id}:* (string) window automation id
        *${found_index}:* (optional) custom control automation id
        == Returns ==
        (str) help text value of custom contol
        """
        viewer = ui.WindowControl(AutomationId=window_id, foundIndex=found_index)
        return viewer.PaneControl(Name=panel_name, foundIndex=found_index).Exists()

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item)
    def existed_status_of_control_name_on_window_id(control_name, window_id, found_index=1) -> bool:
        """
        == Description ==
        Get existed status of panel name on the window id
        == Arguments ==
        *${control_name}:* (string) control name
        *${window_id}:* (string) window automation id
        *${found_index}:* (optional) custom control automation id
        == Returns ==
        (str) help text value of custom contol
        """
        window = ui.WindowControl(AutomationId=window_id, foundIndex=found_index)
        return window.Control(Name=control_name, foundIndex=found_index).Exists()

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item)
    def value_by_document_control_id(document_control_id, panel_name, found_index=1) -> str:
        """
        == Description ==
        Get document control's value which is chid of pane name
        == Arguments ==
        *${document_control_id}:* (string) document control id
        *${panel_name}:* (string) pane name
        == Returns ==
        (str) document control's value
        """
        pane = ui.PaneControl(Name=panel_name, foundIndex=found_index)
        for child in pane.GetChildren():
            if document_control_id == child.AutomationId:
                return child.GetLegacyIAccessiblePattern().Value
        return ''

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item)
    def values_of_pane_id(pane_id, found_index=1) -> list:
        """
        == Description ==
        Get values of pane by automation id
        == Arguments ==
        *${pane_id}:* (string) pane automation id
        == Returns ==
        (list) values of children which are text control
        """
        text_values = []
        pane = ui.PaneControl(AutomationId=pane_id, foundIndex=found_index)
        for child in pane.GetChildren():
            if child.ControlType == ControlType.TextControl:
                text_values.append(child.GetLegacyIAccessiblePattern().Name)
        return text_values

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item)
    def custom_control_exists(custom_automation_id) -> bool:
        """
       == Description ==
       Verify if the custom control exists or not
       == Arguments ==
       *${custom_automation_id}:* (string) custom control automation id
       == Returns ==
       True if exists. Otherwise, return False
       """
        exist_value = ui.CustomControl(AutomationId=custom_automation_id).Exists()
        return exist_value

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item)
    def tooltip_of_custom_control_name() -> str:
        """
        == Description ==
        Get tooltip of custom control by Name attribute
        == Arguments ==
        *None
        == Returns ==
        (str) value of custom control
        """
        return ui.ToolTipControl().Name

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item)
    def window_by_name() -> str:
        """
        == Description ==
        Get the window by Name attribute
        == Arguments ==
        *None*
        == Return ==
        (str) Name of window
        """
        return ui.WindowControl(ClassName='Chrome_WidgetWin_1').Name

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item)
    def window_exists(window_automation_id) -> bool:
        """
        == Description ==
        Verify the window exists or not
        == Arguments ==
        *${window_automation_id}:* (string) window automation id
        == Return ==
        True if exists. Otherwise, return False
        """
        exist_value = ui.WindowControl(AutomationId=window_automation_id).Exists()
        return exist_value

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item)
    def window_name_is_exists(window_name) -> bool:
        """
        == Description ==
        Verify the window exists or not
        == Arguments ==
        *${window_name}:* (string) window expected name
        == Return ==
        True if exists. Otherwise, return False
        """
        return ui.WindowControl(Name=window_name).Exists()

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item)
    def text_box_enabled(text_box_automation_id) -> bool:
        """
        == Description ==
        Check if the textbox is enabled or not.
        == Arguments ==
        *${text_box_automation_id}:* (string) textbox automation id
        == Return ==
        True if textbox is enabled. Otherwise, it returns false
        """
        return ui.TextControl(AutomationId=text_box_automation_id).IsEnabled

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item)
    def state_edit_controls_of_custom_control(automation_id, found_index=1) -> bool:
        """
        == Description ==
        Get state of one-by-one edit control within SmartUtility application.
        If getting failed at the first time, this action will be triggered again.
        == Arguments ==
        *${found_index}:* (optional) the found index
        *${automation_id}:* (string) the custom control id
        == Return ===
        *True* if all edit controls are enabled. Otherwise, it returns *False*
        """
        controls = ui.CustomControl(AutomationId=automation_id, foundIndex=found_index).GetChildren()
        index = 0
        item_state = False
        for item_control in controls:
            if item_control.ClassName == 'TextBox':
                item_state = item_control.IsEnabled
                index += 1
        return item_state

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item)
    def devices_column_information(data_grid_automation_id, column_name) -> list:
        """
        == Description ==
        Get value of the device column
        == Arguments ==
        *${data_grid_automation_id}:* (string) list result automation id
        == Returns ==
        (list) result value
        """
        rows_data = []
        list_results = ui.DataGridControl(AutomationId=data_grid_automation_id)
        list_results.SetFocus()
        rows = list_results.GetChildren()
        headers = rows[0].GetChildren()
        row_index = 1
        while row_index < len(rows):
            column_index = 0
            cells = rows[row_index].GetChildren()
            while column_index < len(cells):
                header_name = headers[column_index].Name
                if header_name.lower() == column_name.lower():
                    cellValue = cells[column_index].GetChildren()[0].Name
                    rows_data.append(cellValue.replace('\n', ''))
                column_index += 1
            row_index += 1
        return rows_data

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item)
    def edit_control_enabled(edit_box_automation_id) -> bool:
        """
        == Description ==
        Check if the edit control is enabled or not.
        == Arguments ==
        *${edit_box_automation_id}:* (string) edit control automation id
        == Return ==
        True if edit control is enabled. Otherwise, it returns false
        """
        return ui.EditControl(AutomationId=edit_box_automation_id).IsEnabled

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item)
    def value_of_editcontrol_by_id(automation_id, index):
        """
        == Description ==
        Get the value of edit-control based on the automation id in SmartUtility application.
        If getting failed at the first time, this action will
        be triggered again.
        == Arguments ==
        *${automation_id}:* (string) the automation id
        *${index}:* (int) the index of control
        == Returns ==
        The value of the edit control
        """
        return ui.EditControl(AutomationId=automation_id,
                              foundIndex=index).GetValuePattern().Value

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item)
    def value_of_editcontrol_by_helptext(automation_id, index):
        """
        == Description ==
        Get the value of edit-control based on the Help Text in SmartUtility application.
        If getting failed at the first time, this action will be triggered again.
        == Arguments ==
        *${automation_id}:* (string) the automation id
        *${index}:* (int) the index of control
        == Returns ==
        The value of the edit control
        """
        return ui.EditControl(AutomationId=automation_id,foundIndex=index).HelpText

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item)
    def name_of_window() -> str:
        return ui.WindowControl().Name

    @staticmethod
    def tooltip_of_custom_control_exists(expected_tooltip) -> bool:
        """
        == Description ==
        Get tooltip of custom control by Name attribute
        == Arguments ==
        *None
        == Returns ==
        *True* if the tooltip exists, otherwise, it returns *False*
        """
        is_exists = ui.ToolTipControl(SubName=expected_tooltip).Exists()
        return is_exists

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item)
    def total_rows(data_grid_automation_id) -> int:
        """
        == Description ==
        Get the total rows in the grid
        == Arguments ==
        *${data_grid_automation_id}:* (string) list result automation id
        == Returns ==
        (int) the total rows in the grid
        """
        list_results = ui.DataGridControl(AutomationId=data_grid_automation_id)
        list_results.SetFocus()
        return len(list_results.GetChildren()) - 1

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item)
    def context_menu_by_classname(className) -> bool:
        """
        == Description ==
        Get the context menu exist or not
        == Arguments ==
        *${className}:* (string) the class name of context menu
        == Returns ==
        True if exists. Otherwise, return False
        """
        return ui.MenuControl(ClassName=className).Exists()

    @staticmethod
    def the_selected_item_in_list_control(item) -> bool:
        """
        == Description ==
        Get the item in the list view is selected or not
        == Arguments ==
        *$item}:* (string) the item in the list
        == Returns ==
        *True* if the item is selected, otherwise, return *False*
        """
        return ui.ListItemControl(AutomationId=item).GetSelectionItemPattern().IsSelected

    @staticmethod
    def button_enabled(button_automation_id) -> bool:
        """
        == Description ==
        Check if the button is enabled or not.
        == Arguments ==
        *${button_automation_id}:* (string) button automation id
        == Return ==
        *True* if button is enabled. Otherwise, it returns *False*
        """
        return ui.ButtonControl(AutomationId=button_automation_id).IsEnabled

    @staticmethod
    def number_items_in_combo_box(combo_box_automation_id, index=1) -> int:
        """
        == Description ==
        Count the number of items in the combo box.
        == Arguments ==
        *${combo_box_automation_id}:* (string) combo box automation id
        *${index}:* (index) the index of combo box automation id
        == Return ==
        *The number of items in the combo box*
        """
        item_count = 0
        items = ui.ComboBoxControl(AutomationId=combo_box_automation_id,foundIndex=index)
        for item in items.GetChildren():
            item_count = item_count + 1
        return item_count

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item)
    def list_item_by_name_or_subname(expected_name) -> bool:
        """
           == Description ==
           Get item of list item control by Name attribute
           == Arguments ==
           *name of item
           == Returns ==s
           (boo) True or False
               """
        is_exist = False
        if ui.TextControl(SubName=expected_name):
            is_exist = True
        return is_exist

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item)
    def toggle_state_of_checkbox_by_name(name_attr) -> int:
        """
        == Description ==
        Get toggle value of checkbox by name attribute
        == Arguments ==
        *${name_attr}:* (string) checkbox's name
        == Returns ==
        *On (1)* if checkbox is checked
        *Off (0)* if checkbox is unchecked
        """
        return ui.CheckBoxControl(Name=name_attr, foundIndex=1).GetTogglePattern().ToggleState

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item)
    def list_of_active_devices_in_grid(grid_id) -> list:
        """
        == Description ==
        Get the List of current active devices in SmartUtility
        == Arguments ==
        grid view id
        == Returns ==
        (boo) value of custom control
        """
        activeDevicesList =[]

        devicesList = ui.DataGridControl(AutomationId=grid_id)

        for device in devicesList.GetChildren() :
            itemList = device.GetChildren()
            itemSubList = itemList[0].GetChildren()
            for item in itemSubList:
                checkboxList = item.GetChildren()
                for checkbox in checkboxList:
                    if checkbox.IsEnabled and checkbox.ClassName=='CheckBox':
                        activeDevicesList.append(device.Name)
        print(activeDevicesList)
        return activeDevicesList