from retry import retry
import pyautogui
import time
from CommonLibrary.actions.Click import Click


class Filter(object):
    """Filter action on Schaeffler application"""

    @staticmethod
    @retry(tries=2)
    def on_device_view_using_characteristic_value(automation_id, characteristic_value) -> None:
        """
        == Description ==
        Filter on device view within SmartUtilityLibrary application.
        If getting failed at the first time, this action will
        be triggered again.
        == Arguments ==
        *${automation_id}:* (string) the filter control id
        *${characteristic_value}:* (string) the input characteristic value
        """
        time.sleep(6)
        Click.on_filter_using_automation_id(automation_id=automation_id)
        time.sleep(1)
        pyautogui.write(characteristic_value)
        time.sleep(1)
        pyautogui.press('enter')
