import uiautomation as ui
import pyautogui
import time
from retry import retry
from CommonLibrary.utils.DelayTimeConfig import delay_time_config
from uiautomation.uiautomation import ToggleState


class Click(object):
    """Click action on Schaeffler application"""

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item, delay=delay_time_config.delay_time_control_item)
    def on_button_using_automation_id(automation_id, found_index: int = 1) -> None:
        """
        == Description ==
        Click on Button control based on automation id within
        CommonLibrary application.
        If getting failed at the first time, this action will
        be triggered again.
        == Arguments ==
        *${automation_id}:* (string) the button id
        *${found_index}:* (optional) the found index of this button
        """
        if not ui.ButtonControl(AutomationId=automation_id, foundIndex=found_index).IsEnabled:
            raise Exception("Button '%ds' is not enabled to click," % automation_id)
        ui.ButtonControl(AutomationId=automation_id, foundIndex=found_index).Click()

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item, delay=delay_time_config.delay_time_control_item)
    def on_button_id_of_window_id(button_id, window_id, found_index: int = 1) -> None:
        """
        == Description ==
        Click on Button control based on automation id within
        CommonLibrary application.
        If getting failed at the first time, this action will
        be triggered again.
        == Arguments ==
        *${button_id}:* (string) the button id
        *${window_id}:* (string) the window id
        *${found_index}:* (optional) the found index of this button
        """
        window = ui.WindowControl(AutomationId=window_id, foundIndex=found_index)
        if not window.ButtonControl(AutomationId=button_id, foundIndex=found_index).IsEnabled:
            raise Exception("Button '%ds' is not enabled to click," % button_id)
        window.ButtonControl(AutomationId=button_id, foundIndex=found_index).Click()
        time.sleep(0.5)

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item, delay=delay_time_config.delay_time_control_item)
    def on_button_of_window_using_name(button_name, window_name, found_index: int = 1) -> None:
        """
        == Description ==
        Click on Button control based on automation name within
        CommonLibrary application.
        If getting failed at the first time, this action will
        be triggered again.
        == Arguments ==
        *${button_name}:* (string) the button name
        *${window_name}:* (string) the window name
        *${found_index}:* (optional) the found index of this button
        """
        window = ui.WindowControl(Name=window_name, foundIndex=found_index)
        window.ButtonControl(Name=button_name, foundIndex=found_index).Click()

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item, delay=delay_time_config.delay_time_control_item)
    def to_deselect_on_data_item_name_by_custom_control_automation_id(data_item_name,
                                                                      select_item_custom_control_automation_id) -> None:
        """
        == Description ==
        Click to de-select on checkbox radio button which is child of custom control automation id
        within CommonLibrary application.
        Only click on checkbox when it is checked
        If getting failed at the first time, this action will
        be triggered again.
        == Arguments ==
        *${data_item_name}:* (string) data item name
        *${select_item_custom_control_automation_id}:* (string) custom control automation id of select item
        """
        data_item = ui.DataItemControl(Name=data_item_name).CustomControl(
            AutomationId=select_item_custom_control_automation_id)
        if data_item.Name == 'True':
            data_item.Click()

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item, delay=delay_time_config.delay_time_control_item)
    def to_select_on_data_item_name_by_custom_control_automation_id(data_item_name,
                                                                    select_item_custom_control_automation_id) -> None:
        """
        == Description ==
        Click on checkbox/ or radio button which are chidren of custom control automation id within
        CommonLibrary application.
        For checkbox: only click on checkbox when it is un-checked
        If getting failed at the first time, this action will
        be triggered again.
        == Arguments ==
        *${data_item_name}:* (string) data item name
        *${select_item_custom_control_automation_id}:* (string) custom control automation id of select item
        """
        data_item = ui.DataItemControl(Name=data_item_name).CustomControl(
            AutomationId=select_item_custom_control_automation_id)
        if data_item.Name == 'False':
            if not data_item.CheckBoxControl(searchDepth=1).IsEnabled:
                raise Exception("Item is not enabled to click")
            else:
                data_item.Click()

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item, delay=delay_time_config.delay_time_control_item)
    def to_select_on_data_item_name_by_check_box_automation_id(data_item_name, check_box_automation_id) -> None:
        """
        == Description ==
        Click on checkbox/ or radio button which are chidren of custom control automation id within
        CommonLibrary application.
        For checkbox: only click on checkbox when it is un-checked
        If getting failed at the first time, this action will
        be triggered again.
        == Arguments ==
        *${data_item_name}:* (string) data item name
        *${select_item_custom_control_automation_id}:* (string) custom control automation id of select item
        """
        check_box = ui.DataItemControl(AutomationId=data_item_name).CheckBoxControl(
            AutomationId=check_box_automation_id)
        if check_box.GetTogglePattern().ToggleState == ToggleState.Off:
            if not check_box.IsEnabled:
                raise Exception("Item is not enabled to click")
            else:
                check_box.Click()

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item, delay=delay_time_config.delay_time_control_item)
    def to_select_on_group_control(data_item_name) -> None:
        """
        == Description ==
        Click on checkbox/ or radio button which are children of custom control automation id within
        CommonLibrary application.
        For checkbox: only click on checkbox when it is un-checked
        If getting failed at the first time, this action will
        be triggered again.
        == Arguments ==
        *${data_item_name}:* (string) data item name
        *${select_item_custom_control_automation_id}:* (string) custom control automation id of select item
        """
        group_item = ui.GroupControl(Name=data_item_name)
        signal_item = group_item.TextControl(AutomationId='ListViewSubItem-0')
        if signal_item.Name == 'False':
            signal_item.Click()

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item)
    def on_textbox_using_automation_id(automation_id, found_index: int = 1) -> None:
        """
        == Description ==
        Click on textbox control based on automation id within
        CommonLibrary application.
        If getting failed at the first time, this action will
        be triggered again.
        == Arguments ==
        *${found_index}:* (optional) the found index
        *${automation_id}:* (string) the textbox id
        """
        ui.EditControl(AutomationId=automation_id, foundIndex=found_index).Click()

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item)
    def on_filter_using_automation_id(automation_id) -> None:
        """
        == Description ==
        Click on filter control based on automation id within
        CommonLibrary application.
        If getting failed at the first time, this action will
        be triggered again.
        == Arguments ==
        *${automation_id}:* (string) the filter control id
        """
        ui.PaneControl(AutomationId=automation_id).Click()

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item)
    def on_checkbox_using_automation_id(automation_id, found_index: int = 1) -> None:
        """
        == Description ==
        Click on checkbox control based on automation id within
        CommonLibrary application.
        If getting failed at the first time, this action will
        be triggered again.
        == Arguments ==
        *${found_index}:* (optional) the found index
        *${automation_id}:* (string) the checkbox id
        """
        ui.CheckBoxControl(AutomationId=automation_id, foundIndex=found_index).Click()

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item)
    def on_list_item_using_name(name, found_index: int = 1) -> None:
        """
        == Description ==
        Click on List-Item control based name
        within CommonLibrary application.
        If getting failed at the first time, this action will
        be triggered again.
        == Arguments ==
        *${found_index}:* (optional) the found index
        *${name}:* (string) the item name
        """
        ui.ListItemControl(Name=name, foundIndex=found_index).Click()

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item)
    def on_radio_button_of_list_item_using_automation_id(automation_id, found_index: int = 1) -> None:
        """
        == Description ==
        Click on radio button
        within CommonLibrary application.
        If getting failed at the first time, this action will
        be triggered again.
        == Arguments ==
        *${found_index}:* (optional) the found index
        *${automation_id}:* (string) id of list item which is parent of radio button
        """
        ui.ListItemControl(AutomationId=automation_id, foundIndex=found_index).RadioButtonControl(foundIndex=1).Click()

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item)
    def on_item_of_combo_box_using_automation_id(item_name, combo_box_automation_id, found_index: int = 1) -> None:
        """
        == Description ==
        Click on one item of combo box by name
        within CommonLibrary application.
        If getting failed at the first time, this action will
        be triggered again.
        == Arguments ==
        *${found_index}:* (optional) the found index
        *${combo_box_automation_id}:* (string) id of combo box
        *${item_name}:* (string) id of item name to be selected
        """
        ui.ComboBoxControl(AutomationId=combo_box_automation_id, foundIndex=found_index).Select(item_name)

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item)
    def on_item_of_list_view_using_automation_id(item_name, list_view_automation_id, found_index=1) -> None:
        """
        == Description ==
        Click on one item of list view by name
        within CommonLibrary application.
        If getting failed at the first time, this action will
        be triggered again.
        == Arguments ==
        *${found_index}:* (optional) the found index
        *${list_view_automation_id}:* (string) id of list view
        *${item_name}:* (string) id of item name to be selected
        """
        ui.ListControl(AutomationId=list_view_automation_id, foundIndex=found_index).ListItemControl(Name=item_name,
                                                                                                     foundIndex=found_index).Click()

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item)
    def on_radio_button_using_automation_id(automation_id, found_index: int = 1) -> None:
        """
        == Description ==
        Click on radio button by its automation id
        within CommonLibrary application.
        If getting failed at the first time, this action will
        be triggered again.
        == Arguments ==
        *${found_index}:* (optional) the found index
        *${automation_id}:* (string) id of radio button
        """
        ui.RadioButtonControl(AutomationId=automation_id, foundIndex=found_index).Click()

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item)
    def on_radio_button_name(radio_button_name, pane_control_id, found_index: int = 1) -> None:
        pane = ui.PaneControl(AutomationId=pane_control_id, foundIndex=found_index)
        pane.RadioButtonControl(Name=radio_button_name, foundIndex=found_index).Click()

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item)
    def on_menu_item_using_automation_id(automation_id, found_index: int = 1) -> None:
        """
        == Description ==
        Click on MenuItem control based on automation id within
        CommonLibrary application.
        If getting failed at the first time, this action will
        be triggered again.
        == Arguments ==
        *${automation_id}:* (string) the menu item id
        *${found_index}:* (optional) the found index of this button
        """
        if not ui.MenuItemControl(AutomationId=automation_id, foundIndex=found_index).IsEnabled:
            raise Exception("MenuItem '%ds' is not enabled to click," % automation_id)
        ui.MenuItemControl(AutomationId=automation_id, foundIndex=found_index).Click()

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item)
    def on_tree_view_item_using_automation_id(automation_id, found_index: int = 1) -> None:
        """
        == Description ==
        Click on TreeViewItem control based on automation id within
        CommonLibrary application.
        If getting failed at the first time, this action will
        be triggered again.
        == Arguments ==
        *${automation_id}:* (string) the tree view item id
        *${found_index}:* (optional) the found index of this button
        """
        if not ui.TreeItemControl(AutomationId=automation_id, foundIndex=found_index).IsEnabled:
            raise Exception("TreeItemControl '%ds' is not enabled to click," % automation_id)
        ui.TreeItemControl(AutomationId=automation_id, foundIndex=found_index).Click()

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item)
    def on_button_name_of_panel(button_name, panel_name) -> None:
        """
        == Description ==
        Click on button name of panel name
        CommonLibrary application.
        If getting failed at the first time, this action will
        be triggered again.
        == Arguments ==
        *${button_name}:* (string) the button name
        *${panel_name}:* (string) the panel name
        """
        button = ui.PaneControl(Name=panel_name).ButtonControl(Name=button_name)
        button.Click()

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item)
    def on_list_item_using_names(list_item_name, scroll_up_button_name, scroll_down_button_name, panel_name,
                                 tree_id) -> None:
        """
        == Description ==
        Click on the list item. If the item is not display, \n
        the scroll bar will move up or move down \n
        CommonLibrary application. \n
        If getting failed at the first time, this action will \n
        be triggered again. \n
        == Arguments ==
        *${list_item_name}:* (string) the item name which is selected \n
        *${main_window_id}:* (optional) the main window id of CommonLibrary application \n
        *${window_name}:* (optional) the window name of CommonLibrary application \n
        *${device_view_panel_name}:* (optional) the name of device view panel \n
        *${scroll_up_button_name}:* (optional) the scroll down button name \n
        *${scroll_down_button_name}:* (optional) the scroll up button name \n
        *${tree_id}:* (optional) list control automation id \n
        """
        names = list_item_name.split(' > ')
        tree = ui.PaneControl(Name=panel_name).ListControl(AutomationId=tree_id)
        tree_index = 0
        characteristic_indexes = []
        displayed_indexes = []
        tree_items = tree.GetChildren()
        while tree_index < len(tree_items):
            if tree_items[tree_index].ControlType == ui.ControlType.ListItemControl:
                item_text = tree_items[tree_index].TextControl(searchDepth=1)
                if item_text.BoundingRectangle.bottom != 0:
                    displayed_indexes.append(tree_index)
                if (len(characteristic_indexes) < len(names)) \
                        and (item_text.Name.lower() == names[len(characteristic_indexes)].lower()):
                    characteristic_indexes.append(tree_index)
            tree_index += 1

        if len(characteristic_indexes) == len(names):
            # scroll to position of child_index
            child_index = characteristic_indexes[len(characteristic_indexes) - 1]
            while tree_items[child_index].TextControl(searchDepth=1).BoundingRectangle.bottom == 0:
                if child_index < displayed_indexes[0]:
                    Click.on_button_name_of_panel(scroll_up_button_name, panel_name)
                elif child_index > displayed_indexes[len(displayed_indexes) - 1]:
                    Click.on_button_name_of_panel(scroll_down_button_name, panel_name)
            # click on item
            tree_items[child_index].Click()
        else:
            raise Exception(
                "Characteristic value '%s' is not found, click expand button" % names[len(characteristic_indexes)])

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item)
    def on_button_name_of_window_id(button_name, window_id, found_index=1) -> None:
        """
        Click on button name of window automation id
        == Arguments ==
        *${main_window_id}:* (optional) the main window id of CommonLibrary application \n
        *${window_name}:* (optional) the window name of CommonLibrary application \n
        *${close_button_name}:* (optional) the close button name \n
        *${viewer_window_id}:* (optional) the viewr window id \n
        """
        window = ui.WindowControl(AutomationId=window_id, foundIndex=found_index)
        window.ButtonControl(Name=button_name, foundIndex=found_index).Click()

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item)
    def item_of_list_control(view_panel_name, list_control_id, search_criterias_dict) -> None:
        """
        Select time stamp to view the time signals. \n
        == Arguments ==
        *${time_stamp}:* (string) the input characteristic value \n
        *${search_criteria_dict}:* (dict) dictionary for searching criteria \n
        *${list_time_signals_id}:* (string) list control automation id \n
        """
        list_results = ui.PaneControl(Name=view_panel_name).ListControl(AutomationId=list_control_id)
        list_results.SetFocus()
        rows = list_results.GetChildren()

        # check keys of search criteria
        headers = []
        for header in rows[0].GetChildren():
            headers.append(header.Name)
        for criteria_key in search_criterias_dict.keys():
            if criteria_key not in headers:
                raise Exception("Not found '%s' in headers" % criteria_key)

        # check values of search criteria
        row_index = 1
        target_row_index = -1
        while row_index < len(rows):
            target_row_index = row_index
            cells = rows[row_index].GetChildren()
            for criteria_key in search_criterias_dict.keys():
                column_index = headers.index(criteria_key)
                if cells[column_index].Name != search_criterias_dict[criteria_key]:
                    target_row_index = -1
                    break
            if target_row_index != -1:
                rows[row_index].Click()
                return
            else:
                row_index += 1

        raise Exception("Not found any item with %s" % search_criterias_dict)

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item)
    def item_of_group_control(view_panel_name, list_control_id, search_criterias_dict) -> None:
        """
        Select time stamp to view the time signals. \n
        == Arguments ==
        *${time_stamp}:* (string) the input characteristic value \n
        *${search_criteria_dict}:* (dict) dictionary for searching criteria \n
        *${list_time_signals_id}:* (string) list control automation id \n
        """
        list_results = ui.PaneControl(Name=view_panel_name).ListControl(AutomationId=list_control_id)
        list_results.SetFocus()
        rows = list_results.GetChildren()

        # check keys of search criteria
        headers = []
        for header in rows[0].GetChildren():
            headers.append(header.Name)
        for criteria_key in search_criterias_dict.keys():
            if criteria_key not in headers:
                raise Exception("Not found '%s' in headers" % criteria_key)

        # check values of search criteria
        row_index = 0
        signal_rows = rows[1].GetChildren()
        while row_index < len(signal_rows):
            cells = signal_rows[row_index].GetChildren()
            for criteria_key in search_criterias_dict.keys():
                search_values = search_criterias_dict[criteria_key].split('|')
                column_index = headers.index(criteria_key)
                if cells[column_index].Name in search_values:
                    cells[0].Click()

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item)
    def pane_name_by_right_click(pane_name) -> None:
        """
        Right-click on the pane name \n
        == Arguments ==
        *${pane_name}:* (string) the pane name \n
        """
        ui.PaneControl(Name=pane_name).RightClick()

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item)
    def menu_name_on_context_menu(menu_name, context_menu_name, window_id) -> None:
        """
        Click on the menu name to open
        == Arguments ==
        *${menu_name}:* (string) menu name. Ex: Settings..., Export > Save diagram (RTF format)... \n
        *${context_menu_name}:* (string) context menu name \n
        *${window_id}:* (string) window automation id \n
        """
        names = menu_name.split(' > ')
        menu = ui.WindowControl(AutomationId=window_id).MenuControl(Name=context_menu_name)
        for name in names:
            menu = menu.MenuItemControl(Name=name)
            menu.Click()

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item)
    def on_radio_button_using_name(window_id, radio_button_name, found_index=1) -> None:
        window = ui.WindowControl(AutomationId=window_id, foundIndex=found_index)
        window.RadioButtonControl(Name=radio_button_name, foundIndex=found_index).Click()

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item)
    def on_item_of_list_view(item_name, list_view_automation_id) -> None:
        """
        == Description ==
        Click on one item of list view by automation id of item name within CommonLibrary application.
        If getting failed at the first time, this action will be triggered again.
        == Arguments ==
        *${list_view_automation_id}:* (string) id of list view
        *${item_name}:* (string) id of item name to be selected
        """
        ui.ListControl(AutomationId=list_view_automation_id, foundIndex=1).ListItemControl(
            AutomationId=item_name).Click()

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item)
    def on_combo_box_using_automation_id(combo_box_automation_id, found_index: int = 1) -> None:
        """
        == Description ==
        Click combo box by automation id within CommonLibrary application.
        If getting failed at the first time, this action will
        be triggered again.
        == Arguments ==
        *${found_index}:* (optional) the found index
        *${combo_box_automation_id}:* (string) id of combo box
        """
        ui.ComboBoxControl(AutomationId=combo_box_automation_id, foundIndex=found_index).Click()

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item)
    def on_item_on_tree_view_using_name(item) -> None:
        ui.TreeItemControl(Name=item).Click()

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item)
    def on_treeview_open_context_menu_using_automation_id(menu_name_automation_id, context_menu_id,
                                                          tree_view_item_id) -> None:
        """
        Right click to open context menu
        == Arguments ==
        *${menu_name_automation_id}:* (string) automation id of menu name.\n
        *${context_menu_id}:* (string) context menu automation id \n
        *${tree_view_item_id}:* (string) tree view item automation id \n
        """
        ui.TreeItemControl(AutomationId=tree_view_item_id).Click()
        position = ui.TextControl(Name=tree_view_item_id).GetPosition()
        pyautogui.moveTo(position[0], position[1])
        pyautogui.rightClick()
        time.sleep(0.5)
        ui.MenuControl(AutomationId=context_menu_id).MenuItemControl(AutomationId=menu_name_automation_id).Click()

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item)
    def on_listview_open_context_menu_using_automation_id(listview_automation_id, menu_item_automation_id) -> None:
        """
        Right click on list to open the context menu
        == Arguments ==
        *${listview_automation_id}:* (string) automation id of list view.\n
        """
        ui.DataGridControl(AutomationId=listview_automation_id).RightClick()
        ui.MenuItemControl(AutomationId=menu_item_automation_id).Click()

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item)
    def on_list_view_header(list_view_header_id) -> None:
        """
        == Description ==
        Click on list view header within SmartUtility application.
        If getting failed at the first time, this action will be triggered again.
        == Arguments ==
        *${list_view_header_id}:* (string) the list view header id
        """
        ui.HeaderItemControl(AutomationId=list_view_header_id).Click()
        time.sleep(0.5)

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item)
    def on_listview_open_context_menu_by_using_automation_id(list_view_automation_id) -> None:
        """
        Right click to open context menu
        == Arguments ==
        *${list_view_automation_id}:* (string) automation id of listview.\n
        *${context_menu_id}:* (string) context menu automation id \n
        """
        ui.ListControl(AutomationId=list_view_automation_id).Click()
        position = ui.ListControl(AutomationId=list_view_automation_id).GetPosition()
        pyautogui.moveTo(position[0], position[1])
        pyautogui.rightClick()
        time.sleep(0.5)

    @staticmethod
    def on_list_item_open_context_menu_using_automation_id(list_view_item_id) -> None:
        """
        Right click on list item to open context menu
        == Arguments ==
        *${list_view_automation_id}:* (string) automation id of listview.\n
        *${context_menu_id}:* (string) context menu automation id \n
        """
        ui.ListItemControl(AutomationId=list_view_item_id).Click()
        position = ui.ListItemControl(AutomationId=list_view_item_id).GetPosition()
        pyautogui.moveTo(position[0], position[1])
        pyautogui.rightClick()

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item)
    def de_select_on_item_of_list_view(item_name, list_view_automation_id) -> None:
        """
        == Description ==
        Ctrl - Click on one item of list view by automation id of item name within SmartUtility application.
        If getting failed at the first time, this action will be triggered again.
        == Arguments ==
        *${list_view_automation_id}:* (string) id of list view
        *${item_name}:* (string) id of item name to be selected
        """
        ui.ListControl(AutomationId=list_view_automation_id, foundIndex=1).ListItemControl(
            AutomationId=item_name).Click()
        pyautogui.keyDown('ctrl')
        pyautogui.click()
        pyautogui.keyUp('ctrl')

    @staticmethod
    def press_on_combo_box_using_automation_id(combo_box_automation_id, found_index: int = 1) -> None:
        """
        == Description ==
        Open the combo box by key
        == Arguments ==
        *${combo_box_automation_id}:* (string) id of combo box
        *${found_index}:* (optional) the found index
        """
        ui.ComboBoxControl(AutomationId=combo_box_automation_id, foundIndex=found_index).Click()
        pyautogui.keyDown('f4')
        pyautogui.keyUp('f4')

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item)
    def on_checkbox_using_name(name_attr, found_index: int = 1) -> None:
        """
        == Description ==
        Click on checkbox control based on name within SmartUtility application.
        If getting failed at the first time, this action will be triggered again.
        == Arguments ==
        *${found_index}:* (optional) the found index
        *${name_attr}:* (string) the checkbox's name
        """
        ui.CheckBoxControl(Name=name_attr, foundIndex=found_index).Click()

