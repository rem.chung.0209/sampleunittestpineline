import logging

from retry import retry
import uiautomation as ui
from CommonLibrary.utils.DelayTimeConfig import delay_time_config
import pyautogui


class Enter(object):
    """Enter action on SmartUtility application"""

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item)
    def on_textbox_using_automation_id(automation_id, value, found_index: int = 1, isEnter=True) -> None:
        """
        == Description ==
        Enter the value in the textbox based on automation id and
        the input value within CommonLibrary application.
        If getting failed at the first time, this action will
        be triggered again.
        == Arguments ==
        *${found_index}:* (optional) the found index
        *${automation_id}:* (string) the textbox id
        *${value}:* (string) the input value
        """
        text_box = ui.EditControl(AutomationId=automation_id, foundIndex=found_index)
        text_box.SendKeys('{ctrl}a{delete}')
        text_box.SendKeys(value)
        logging.info("isEnter: %s", isEnter)
        if isEnter:
            text_box.SendKeys('{enter}')

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item)
    def on_custom_control_using_automation_id(automation_id, value, found_index: int = 1) -> None:
        """
        == Description ==
        Enter text/ key on custom control automation id 
        within CommonLibrary application.
        If getting failed at the first time, this action will
        be triggered again.
        == Arguments ==
        *${found_index}:* (optional) the found index
        *${automation_id}:* (string) the textbox id
        *${value}:* (string) text or key value
        """
        custom_control = ui.CustomControl(AutomationId=automation_id, foundIndex=found_index)
        custom_control.Click()
        custom_control.SendKeys(value)

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item)
    def on_custom_control_using_class_name(class_name, value, found_index: int = 1) -> None:
        """
        == Description ==
        Enter text/ key on custom control by class name
        within CommonLibrary application.
        If getting failed at the first time, this action will
        be triggered again.
        == Arguments ==
        *${found_index}:* (optional) the found index
        *${class_name}:* (string) class name
        *${value}:* (string) text or key value
        """
        custom_control = ui.CustomControl(ClassName=class_name, foundIndex=found_index)
        custom_control.Click()
        custom_control.SendKeys(value)

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item)
    def on_window_using_window_automation_id(automation_id, value, found_index: int = 1) -> None:
        """
        == Description ==
        Enter text/ key on window
        within CommonLibrary application.
        If getting failed at the first time, this action will
        be triggered again.
        == Arguments ==
        *${found_index}:* (optional) the found index
        *${automation_id}:* (string) window automation id
        *${value}:* (string) text or key value
        """
        window = ui.WindowControl(AutomationId=automation_id, foundIndex=found_index)
        window.SetFocus()
        window.SendKeys(value)
        pyautogui.press(value)

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item)
    def on_combo_box_using_automation_id(automation_id, value, found_index=1) -> None:
        """
        == Description ==
        Enter text on combo box automation id 
        within CommonLibrary application.
        If getting failed at the first time, this action will
        be triggered again.
        == Arguments ==
        *${found_index}:* (optional) the found index
        *${automation_id}:* (string) the combo box id
        *${value}:* (string) text or key value
        """
        combo_box = ui.ComboBoxControl(AutomationId=automation_id, foundIndex=found_index)
        combo_box.SendKeys(value)

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item)
    def on_edit_controls_of_custom_control(automation_id, values, found_index=1) -> None:
        """
        == Description ==
        Enter each value sequenstially on one-by-one edit control 
        within CommonLibrary application.
        If getting failed at the first time, this action will
        be triggered again.
        == Arguments ==
        *${found_index}:* (optional) the found index
        *${automation_id}:* (string) the combo box id
        *${values}:* (list) list of values to input
        """
        controls = ui.CustomControl(AutomationId=automation_id, foundIndex=found_index).GetChildren()
        index = 0
        for item_control in controls:
            if item_control.ClassName == 'TextBox':
                item_control.SendKeys(values[index])
                index += 1

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item)
    def on_textbox_of_window_using_name(value, edit_control_name, window_name, isEnter=True, found_index=1) -> None:
        """
        == Description ==
        Enter text into text box
        within CommonLibrary application.
        be triggered again.
        == Arguments ==
        *${found_index}:* (optional) the found index
        *${edit_control_name}:* (string) edit control name
        *${window_name}:* (string) window name
        *${value}:* (string) value to input
        """
        window = ui.WindowControl(Name=window_name, foundIndex=found_index)
        window.SetActive()
        edit = window.EditControl(Name=edit_control_name, foundIndex=found_index)
        edit.SendKeys(value)
        if isEnter:
            edit.SendKeys('{enter}')

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item)
    def on_tree_item_using_automation_id_press_key(key, tree_item_automation_id) -> None:
        """
        == Description ==
        Press key on the selected tree item
        == Arguments ==
        *${key}:* (string) key name
        *${tree_item_automation_id}:* (string) tree item automation id
        """
        tree_item_id = ui.TreeItemControl(AutomationId=tree_item_automation_id)
        tree_item_id.Click()
        tree_item_id.SendKeys(key)
