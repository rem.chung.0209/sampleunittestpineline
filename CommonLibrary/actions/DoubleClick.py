import uiautomation as ui
import time
from retry import retry
from CommonLibrary.utils.DelayTimeConfig import delay_time_config


class DoubleClick(object):
    """Double click action on SmartUtility application"""

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item, delay=delay_time_config.delay_time_control_item)
    def on_list_view_header(list_view_header_id) -> None:
        """
        == Description ==
        Double click on list view header within SmartUtility application.
        If getting failed at the first time, this action will be triggered again.
        == Arguments ==
        *${list_view_header_id}:* (string) the list view header id
        """
        ui.HeaderItemControl(AutomationId=list_view_header_id).DoubleClick()
        time.sleep(0.5)
