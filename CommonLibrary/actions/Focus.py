from retry import retry
import uiautomation as ui
from CommonLibrary.utils.DelayTimeConfig import delay_time_config


class Focus(object):
    """Set focus on SmartUtility application"""

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item)
    def on_main_window(window_id, window_name) -> None:
        """
        == Description ==
        Set focus on the main window of CommonLibrary application.
        If getting failed at the first time, this action will
        be triggered again.
        == Arguments ==
        *${window_id}:* (string) the window id of CommonLibrary application
        *${window_name}:* (string) the window name of CommonLibrary application
        """
        main_window = ui.WindowControl(searchDepth=2, Name=window_name,
                                       AutomationId=window_id)
        main_window.SetActive()

    @staticmethod
    @retry(tries=delay_time_config.retries_control_item)
    def on_sub_window(window_id, window_name) -> None:
        """
        == Description ==
        Set focus on the sub window of CommonLibrary application.
        If getting failed at the first time, this action will
        be triggered again.
        == Arguments ==
        *${window_id}:* (string) the window id of CommonLibrary application
        *${window_name}:* (string) the window name of CommonLibrary application
        """
        main_window = ui.WindowControl(searchDepth=2, Name=window_name,
                                       AutomationId=window_id)
        sub_window = main_window.WindowControl(foundIndex=1)
        sub_window.SetActive()
