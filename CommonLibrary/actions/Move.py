from retry import retry
import uiautomation as ui
import pyautogui
import logging
from CommonLibrary.utils.DelayTimeConfig import delay_time_config


class Move(object):
    """Move mouse to the control on SmartUtility application"""

    @retry(tries=delay_time_config.retries_control_item)
    def move_to_control(automation_id, index: int = 1) -> None:
        control_position = ui.CustomControl(AutomationId=automation_id, foundIndex=index).GetClickablePoint()
        logging.info(("X coordinate: %s", control_position[0]))
        logging.info(("Y coordinate: %s", control_position[1]))
        pyautogui.moveTo(control_position[0], control_position[1])
        pyautogui.sleep(1)
