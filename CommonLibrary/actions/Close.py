from retry import retry
import psutil


class Close(object):
    """Close action on SmartUtility application"""

    @staticmethod
    @retry(tries=2)
    def the_application(app_name) -> None:
        """
        == Description ==
        Close SmartUtility application by using the application name.
        If getting failed at the first time, this action will
        be triggered again.
        == Arguments ==
        *${app_name}:* (optional) the application name will be closed
        """
        for proc in psutil.process_iter():
            if str(app_name).lower() in proc.name().lower():
                proc.kill()